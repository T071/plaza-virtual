const innerContainer = document.querySelectorAll('.inner-container')
const images = document.querySelectorAll('.inner-container img')

innerContainer[0].ondragover = (e) => e.preventDefault()
innerContainer[1].ondragover = (e) => e.preventDefault()


images[0].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[1].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[2].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[3].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[4].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[5].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[6].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[7].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[8].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[9].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[10].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[11].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[12].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[13].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[14].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[15].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[16].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)
images[17].ondragstart = (e) => e.dataTransfer.setData("data", e.target.id)



innerContainer[0].ondrop = (e) => drop(e)
innerContainer[1].ondrop = (e) => drop(e)


function drop(e) {
    e.preventDefault()

    var id = e.dataTransfer.getData("data")

    e.target.appendChild(document.getElementById(id))
}